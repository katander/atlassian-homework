## Atlassian Homework

### Quick note
Unfortunately, I wasn't able to open the Sketch file provided with the assignment, but I did my best to match the design in the mockup provided. Apologies in advance for the incorrect fonts, colors, etc.! 

### Running the app
I used a simple Python server to host the page:
`$ python -m SimpleHTTPServer 1338`
[http://localhost:1338/](http://localhost:1338/)
