import { data } from './data.js';

export const load = () => {
    const selectRoot = document.getElementById('filter-select');
    const cardsRoot = document.getElementById('content-posts-cards');

    // Create a map of category => posts[], to prevent looping
    // through the array every time a new category is selected in the dropdown
    const cardsByCategory = {};
    cardsByCategory["any"] = [];
    data.forEach(post => {
        const { category } = post;
        if(!cardsByCategory[category]) {
            cardsByCategory[category] = [];
        }
        cardsByCategory[category].push(post);
    });

    // Loop through the data and gather markup for all the cards
    const cards = getCardsMarkup(data);

    // Create markup for the category dropdown
    const select = getSelectMarkup(cardsByCategory);

    // Append all the content to the root elements
    selectRoot.innerHTML = select;
    cardsRoot.innerHTML = cards;
    
    // Setup a select `change` event listener to filter the cards for a category
    // and replace the cards in the DOM
    document.addEventListener('change', e => {
        if(e.target && e.target.id == 'category-select'){
            const category = e.target.value;
            
            let cards = data; 
            if(category !== "any") {
                cards = cardsByCategory[category];
            }
            
            const filteredCards = getCardsMarkup(cards);
            cardsRoot.innerHTML = filteredCards;
        }
    });
}

/**
 * Gets HTML markup for a list of cards
 * 
 * @param {array} cards 
 * @return {string}
 */
const getCardsMarkup = cards => {
    return cards.reduce((html, card) => {
        return html += getCardMarkup(card);
    }, '');
}

/**
 * Gets HTML markup for a single card
 * 
 * @param {object} card
 * @return {string}
 */
const getCardMarkup = card => {
    const { title, description, link, image } = card;

    return `
        <div class="card">
            <img src="${image}" class="card-image" />
            <div class="card-title">${title}</div>
            <div class="card-description">${description}</div>
            <a href="${link}" target="_blank" class="card-link">Learn more</a>
        </div>
    `;
}

/**
 * Gets HTML markup for the category dropdown
 * 
 * @param {object} categories
 * @return {string}
 */
const getSelectMarkup = categories => {
    let options = '';
    for(const key in categories){
        options += `<option value="${key}">${key}</option>`;
    }

    return `
        <select id="category-select" class="form-control">
            ${options}
        </select>
    `;
}